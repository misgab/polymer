//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
//Para que tome la aplicaciòn empaquetada en determinada version
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('Ejecutando polymer desde node: ' + port);

app.get("/", function(req, res){
  //res.send('Hola Mundo!!');
  res.sendFile("index.html", {root: '.'});
});
